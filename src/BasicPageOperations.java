import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;


public class BasicPageOperations {

	public static void main(String[] args) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","E:\\Selenium\\chromedriver.exe");
		java.lang.Runtime.getRuntime().exec("E:\\Selenium\\Practice AutoIT\\clickalert.exe");
		WebDriver diver=new ChromeDriver();
		diver.get("http://www.lufthansa.com/online/portal/lh/ua/homepage");
		WebElement from=diver.findElement(By.name("originName1"));
		WebElement from_point=diver.findElement(By.name("originCode1"));
		from.click();
		
		Actions actObj=new Actions(diver);
		
//		actObj.keyDown(Keys.DOWN).click(from_point).build().perform();
		
//	    diver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
//		
//		actObj.moveToElement(from_point).click().build().perform();

		
		Select drp=new Select(diver.findElement(By.name("cabin")));
		drp.selectByValue("B");
		Thread.sleep(2000);
		drp.selectByVisibleText("Premium Economy");
		Thread.sleep(2000);
		drp.selectByIndex(0);
	System.out.println("Added Thread");
	}

}
