import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;



public class seleniumAllOperations {

	public static void main(String[] args) throws WebDriverException, IOException {
		WebDriver	driver=null ;
	    try{
			System.setProperty("webdriver.chrome.driver", "E:\\Selenium\\chromedriver.exe");			
			java.lang.Runtime.getRuntime().exec("E:\\Selenium\\Practice AutoIT\\clickalert.exe");
			driver =new ChromeDriver();
			JavascriptExecutor javascript = (JavascriptExecutor) driver;
			javascript.executeScript("alert('Test Case Execution Is started Now..');");
			Thread.sleep(3000);
			driver.switchTo().alert().accept();
        
			//all links in google
//			driver.get("https://www.google.co.in/");
//			List <WebElement> alllinks=driver.findElements(By.tagName("a"));
//			for(WebElement link:alllinks)
//				{
//					System.out.println(link.getText());
//				}
			//mouse actions
			driver.navigate().to("https://www.facebook.com/careers/teams/engineering");
			Actions act=new Actions(driver);
			act.moveToElement(driver.findElement(By.linkText("By Team"))).build().perform();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			Thread.sleep(2000);
			driver.findElement(By.xpath("//div/a[contains(text(),'Marketing')]")).click();
			Thread.sleep(2000);
			
	    	}
	
	    catch(NoSuchElementException nsee)
	    {
	    	System.out.println("Exception Caught in Main"+nsee.toString());
	    }
	    catch(Exception e)
	    {
	    	System.out.println("Exception Caught in Main"+e.toString());
	    }
	    finally
	    {
//	    	FileUtils.copyFile(((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE),new File("E:\\Selenium\\Screenshots\1.jpg"));
	    	
	    	driver.quit();
	    }
	}

}
