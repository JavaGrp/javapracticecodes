import java.util.Scanner;


public class JavaProgram1 {

	public static void main(String[] args) {
		System.out.println("Enter a number");
		Scanner input=new Scanner(System.in);
		int n=input.nextInt();
		int upper_limit=((n*100)-n)/n;
		System.out.println("The mutiples of 5 till  Upper limit"+upper_limit+" are shown below");
		for(int i=1;i<=upper_limit;i++)
		{
			if(i%5==0)
				System.out.println(i);
		}
	}

}
