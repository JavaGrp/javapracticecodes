import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class myExceptions {
	public static int noOfExceptions=1;	
	
	public static void main(String[] args)  {
		// TODO Auto-generated method stub
		int myarray[]=new int[5];
		try
		{
			 myarray[0]=1;
			 myarray[1]=2;
			 myarray[2]=3;
			 myarray[3]=4;
			 myarray[4]=5;
			String name="";
		myExceptions.userdefined(name);
		myExceptions.OtherExceptions(myarray);
		FileWriter fw=new FileWriter("C:/test.xls");
		fw.write("hi");	
//		System.out.println("Printing last element"+myarray[5]);
		}
//		catch(ArrayIndexOutOfBoundsException aobe)
//		{
//			System.out.println("Make sure array is referenced with proper index");
//			System.out.println("No element is present at that index ");
//		}
		catch (FileNotFoundException foe)
		{
			System.out.println("File is not present in the specified path");
			System.out.println(noOfExceptions++);
		}
		catch(Exception e)
		{
			System.out.println("Exception caught in main()");
			System.out.println(noOfExceptions++);
		}
		int sum=0;
		sum= myarray[0]+ myarray[1]+ myarray[2]+ myarray[3]+myarray[4];
		System.out.println("Sum of all the element is :"+sum);
		try
		{ 	
			int c=4/0;
			System.out.println(c);
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
			System.out.println(noOfExceptions++);
		}
	}
	public static void OtherExceptions(int[] myarray) throws ArrayIndexOutOfBoundsException
	{
	try{
	System.out.println("inside throws");
	System.out.println(myarray[5]);
	}
	catch(NullPointerException e)
	{
		System.out.println(" caught Inside catch Exception-> method 2 "+e.toString());
		System.out.println(noOfExceptions++);
	}
	}
	public static void userdefined(String name) throws Reporterror
	{
		try{
		if(name.equals(""))
			
			throw new Reporterror("Wrong value is passed for Name",name);
		}
		catch(Exception e)
		{
			System.out.println("Inside called method 1 "+e.toString());
			System.out.println(noOfExceptions++);
		}
	}
}
